steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE account (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(100) NOT NULL,
            full_name VARCHAR(100) NOT NULL,
            password VARCHAR(250) NOT NULL,
            height_in_inches INTEGER,
            weight_in_pounds INTEGER,
            sex VARCHAR(10)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE account;
        """,
    ],
]
