from pydantic import BaseModel
from models.exercises import ExerciseOut
from datetime import datetime
from typing import Optional


class ExerciseSliceIn(BaseModel):
    exercise_id: int
    sets: int
    reps: int


class ExerciseSliceOut(BaseModel):
    id: int
    exercise: ExerciseOut
    sets: int
    reps: int
    is_complete: Optional[bool]
    completed_on: Optional[datetime]


class ExerciseSliceUpdate(BaseModel):
    exercise_id: int
    sets: int
    reps: int
    is_complete: Optional[bool]
    completed_on: Optional[datetime]
