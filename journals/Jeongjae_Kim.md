
# Day 1 (W14D1): 02-27-2024 FAST API BACKEND AUTH JWTDOWN
## Started
The team started the project.
## Backend AUTHENTICATION in progress
The team first decided to work on AUTHENTICATION and started coding.



# Day 2 (W14D2): 02-28-2024 FAST API BACKEND AUTH JWTDOWN
## Backend AUTHENTICATION in progress
The team conducted mob programming in turn. One team member shared the screen with guidance, and the other team member worked on coding. Another team members checked the progress, shared opinions about the codes, and waited for their turn by collaborating the process.



# Day 3 (W14D3): 02-29-2024 FAST API BACKEND AUTH JWTDOWN
## Backend AUTHENTICATION user account create, update, & delete
The team kept coding for for the AUTHENTICATION part and completed the major part of it. The team conducted the process as the same style as yesterday's. The team met some blockers but could solve it by working together.
## Adding elements for corresponding "account" table
The team added the same elements in the table in migration to make it corresponding to queries.
## Solving issues with "git push" and "branches"
The team worked with a SEIR to solve the "git push" issue with "branches."



# Day 4 (W14D4): 03-01-2024 STORIES BRAINSTORMING & API
All backend authentication is done on the previous day.
The team decided to work on the stories next week, so the team had stand-up meeting with brainstorming of it.

## Story:
As a...
I want...
So that...

I am Jeongjae. As a 68 inches & 150 pounds mid-40s male, I am seeking for lean-mass-up. During the pandemic, my cardio-ability went down a lot based on the Apple Watch measured data. Meanwhile I kept working out at home by using dumbbells and pull-up bar, so my body weight up from 160lbs to 175lbs. Since early 2023, I started running, so I ran over 60 miles per month in average last year. Because of that, however, my body weight went down from 175lbs to 140lbs. Now I am trying to recover my weight with muscle, but it is not easy to do it. I am looking for an exercise app that can motivate myself to accomplish the goal.

### Feature:
Given...
When...
Then...
And...

### Description:
...

### Screenshots:
...

### Update information for a logged-in user
As a logged-in and/or signed-in user, I'd like to change the details of my information in the app's db such as my full name, height, or weight.

## API
The team started working on API (exercises).



# Day 5 (W15D1): 03-04-2024
The team decided to keep working on exercises databases.



# Day 6 (W15D2): 03-05-2024
The team finished exercises yesterday.
The team decided to keep working on exercise_slice: get one and get all.
We met some blockers such as error handlers, but so far we have been handling those blockers good.



# Day 7 (W15D3): 03-06-2024
The team decided to keep working on exercise_slice: update.
To-do today: Error handling & workouts today.
Tomorrow: workout & workoutSlices



# Day 8 (W15D4): 03-07-2024
To-do today: workout & workoutSlices
Done: workout
In-progress: workoutSlices



# Day 9 (W15D5): 03-08-2024
workoutSlices
In-progress: Move on to FrontEnd



# Day 10 (W16D1): 03-11-2024
Error handling Done
In-progress: Move on to FrontEnd - Redux



# Day 11 (W16D2): 03-12-2024
To-do today: Working on React
Team split up (Abrahim): log-in & log-out



# Day 12 (W16D3): 03-13-2024
To-do today: log-out
Team split up (Tayler, Abrahim): WorkoutList, CreateWorkout



# Day 13 (W16D4): 03-14-2024
Team split up (Alex): WorkoutSlice (note incomplete)
In-progress: Git Merge issue (help from SEIRs)



# Day 14 (W17D1): 03-18-2024
Morning: Practice test
Done: Git Merge (help from SEIRs)
Team split up (Abrahim, Andrew): CalendarView error fix



# Day 15 (W17D2): 03-19-2024
Jury Duty: I missed most of the day.
"SWOLEDAWGS"
Nav bar Re-rendering: fixed by Abrahim



# Day 16 (W17D3): 03-20-2024
To-do: Database, Redux, README, Unit Testing
Team split up (Andrew): README, Unit Testing (watch the lecture video clip)



# Day 17 (W17D4): 03-21-2024
To-do: Redux, workout for global user, Unit Testing
Team split up (Andrew): edit README, Unit Testing (watch the lecture video clip)



# Day 18 (W17D5): 03-22-2024
Team split up (Andrew): edit Journal, Unit Testing
To-do: Unit Testing - Done, Black, Flack8, journals
