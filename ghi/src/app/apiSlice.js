import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"


export const swoleApi = createApi({
    reducerPath: 'swoleApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_API_HOST,
        credentials: 'include',
    }),
    endpoints: (builder) => ({
        getExerciseList: builder.query({
            query: () => '/api/exercises',
        }),
        getUserToken: builder.query({
            query: () => ({
                url: '/token',
            }),
            providesTags: ['Account']
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
            }),
            invalidatesTags: ['Account']
        }),
        getExercise: builder.query({
            query: (id) => `/api/exercises/${id}`,
        }),
        createAccount: builder.mutation({
            query: (account) => ({
                url: 'api/accounts',
                method: 'POST',
                body: account,
            }),
        }),
        getWorkout: builder.query({
            query: (id) => `/api/workouts/${id}`,
        }),
        getWorkoutSliceDetails: builder.query({
            query: (id) => `api/workout_slice/${id}`,
        }),

        getWorkoutSliceList: builder.query({
            query: () => ({
                url: `api/workout_slice/`,
            }),
            providesTags: ['WorkoutSlice']
        }),

        postWorkoutSlice: builder.mutation({
            query: (data) => ({
                url: 'api/workout_slice',
                method: 'POST',
                body: {
                    workout_id: data.id,
                    date_complete: new Date().toISOString(),
                    notes: data.notes,
                    scheduled: data.scheduled,
                    is_complete: true,
                },
            }),
            invalidatesTags: ['WorkoutSlice', 'LastWorkout']
        }),
        login: builder.mutation({
            query: info => {
                let formData = null
                if (info instanceof HTMLElement) {
                    formData = new FormData(info)
                } else {
                    formData = new FormData()
                    formData.append('username', info.email)
                    formData.append('password', info.password)
                }
                return {
                    url: '/token',
                    method: 'POST',
                    body: formData,
                    credentials: 'include',
                }
            },
            invalidatesTags: result => {
                return (result && ['Account']) || []
            },
        }),
        getExerciseSlice: builder.query({
            query: (id) => `/api/exercise/slice/${id}`,
        }),
        createWorkout: builder.mutation({
            query: (workout) => ({
                url: 'api/workouts',
                method: 'POST',
                body: workout,
            }),
            invalidatesTags: ['Workouts']
        }),
        createExerciseSlice: builder.mutation({
            query: (exercise_slice) => ({
                url: 'api/exercise_slice',
                method: 'POST',
                body: exercise_slice,
                credentials: 'include'
            }),
        }),
        getWorkoutList: builder.query({
            query: () => ({
                url: '/api/workouts',
            }),
            providesTags: ['Workouts']
        }),
        getLastWorkout: builder.query({
            query: () => ({
                url: `/api/workout_slice/users_last`,
            }),
            providesTags: ['LastWorkout']
        }),
    }),
})
export const {
    useGetLastWorkoutQuery,
    useGetExerciseListQuery,
    useGetUserTokenQuery,
    useGetExerciseQuery,
    useLoginMutation,
    useLogoutMutation,
    useCreateAccountMutation,
    useGetWorkoutQuery,
    useGetWorkoutSliceDetailsQuery,
    usePostWorkoutSliceMutation,
    useCreateExerciseSliceMutation,
    useGetExerciseSliceQuery,
    useCreateWorkoutMutation ,
    useGetWorkoutSliceListQuery,
    useGetWorkoutListQuery
} = swoleApi
