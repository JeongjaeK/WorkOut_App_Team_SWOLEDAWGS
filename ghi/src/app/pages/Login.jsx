import { useState, useEffect } from "react";
import { useLoginMutation, useGetUserTokenQuery } from "../apiSlice";
import { useNavigate } from "react-router-dom";

const Login = () => {
    const [login, loginStatus] = useLoginMutation()
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('')
    const { data: account, isLoading} = useGetUserTokenQuery()

    const navigate = useNavigate()

    useEffect(() => {
        if (account?.account && !isLoading) navigate('/') 
    }, [account, isLoading])

    useEffect(() => {
        //if (loginStatus.isSuccess) navigate('/')
        if (loginStatus.error) setErrorMessage("Invalid username or password")
    }, [loginStatus])

    const handleSubmit = (e) => {
        e.preventDefault()
        login({
            email: username,
            password
        })
    }
    return (
        <div className="container-lg">
        <div className="card text-bg-light my-3">
            <h5 className="card-header">Login</h5>
            {errorMessage && <div className="alert alert-danger">
                {errorMessage}
            </div>}
            <div className="card-body">
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className="mb-3">
                        <label className="form-label">Username:</label>
                        <input
                            name="username"
                            type="text"
                            className="form-control"
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Password:</label>
                        <input
                            name="password"
                            type="password"
                            className="form-control"
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div>
                        <input className="btn btn-primary" type="submit" value="Login" />
                    </div>
                </form>
            </div>
        </div>
        </div>
    );
};
export default Login;
