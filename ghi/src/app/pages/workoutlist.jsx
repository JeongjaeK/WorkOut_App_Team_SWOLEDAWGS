import { useEffect, useState } from 'react'
import { useGetWorkoutListQuery, useGetUserTokenQuery } from '../apiSlice'
import { useNavigate } from 'react-router'

const WorkoutList = () => {
    const navigate = useNavigate()

    const { data: workouts, isLoading } = useGetWorkoutListQuery()
    const { data: account, isLoading: accountLoading } = useGetUserTokenQuery()
    const [ parsedWorkouts, setParsedWorkouts ] = useState([])


    useEffect(() => {
        if (!isLoading && workouts?.length && !accountLoading && account) {
            const workoutsForUser = workouts.filter(
                (singleWorkout) =>
                    (singleWorkout.user_id === Number(account.account.id) || singleWorkout.user_id === null )
            )
            setParsedWorkouts(workoutsForUser)
        }
    }, [isLoading, workouts, account, accountLoading])


    useEffect(() => {
        if (!account && !isLoading) navigate('/') 
    }, [account, isLoading])


    if (isLoading || accountLoading) return <div>Loading...</div>
    return (
        <>
            <div className="mx-8 container">
                <h1>Workout List</h1>

                <div className="accordion" id="accordionExample">
                    {parsedWorkouts.map((workout) => {
                        return (
                            <div className="accordion-item" key={workout.id}>
                                <h2 className="accordion-header">
                                    <button
                                        className="accordion-button"
                                        type="button"
                                        data-bs-toggle="collapse"
                                        data-bs-target={`#${workout.id}`}
                                        aria-expanded="false"
                                        aria-controls={workout.name}
                                    >
                                        {workout.name}
                                    </button>
                                </h2>
                                <div
                                    id={workout.id}
                                    className="accordion-collapse collapse"
                                    data-bs-parent="#accordionExample"
                                >
                                    <div className="accordion-body">
                                        <div className="table-responsive">
                                            <h3>Exercises</h3>
                                            <button
                                                onClick={() =>
                                                    navigate(
                                                        `/workouts/${workout.id}`
                                                    )
                                                }
                                                className="btn btn-primary"
                                            >
                                                Start Workout
                                            </button>
                                            <table className="table align-middle">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Sets/Reps</th>
                                                        <th>Details</th>
                                                        <th>Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {workout.exercises.map(
                                                        (exercise) => {
                                                            return (
                                                                <tr key={exercise.id}>
                                                                    <td>
                                                                        {exercise.exercise.name
                                                                            .charAt(
                                                                                0
                                                                            )
                                                                            .toUpperCase() +
                                                                            exercise.exercise.name.slice(
                                                                                1
                                                                            )}
                                                                    </td>
                                                                    <td>
                                                                        <b>
                                                                            Sets:
                                                                        </b>{' '}
                                                                        {
                                                                            exercise.sets
                                                                        }{' '}
                                                                        <br />
                                                                        <b>
                                                                            Reps:
                                                                        </b>{' '}
                                                                        {
                                                                            exercise.reps
                                                                        }
                                                                    </td>
                                                                    <td>
                                                                        <b>
                                                                            Body
                                                                            group:
                                                                        </b>{' '}
                                                                        {
                                                                            exercise
                                                                                .exercise
                                                                                .body_group
                                                                        }{' '}
                                                                        <br />
                                                                        <b>
                                                                            Body
                                                                            part:
                                                                        </b>{' '}
                                                                        {
                                                                            exercise
                                                                                .exercise
                                                                                .body_part
                                                                        }{' '}
                                                                        <br />
                                                                        <b>
                                                                            Equipment:
                                                                        </b>{' '}
                                                                        {
                                                                            exercise
                                                                                .exercise
                                                                                .equipment
                                                                        }{' '}
                                                                        <br />
                                                                    </td>
                                                                    <td>
                                                                        {exercise.exercise.descriptions
                                                                            .split(
                                                                                '.'
                                                                            )
                                                                            .filter(
                                                                                (
                                                                                    x
                                                                                ) =>
                                                                                    x.length >
                                                                                    1
                                                                            )
                                                                            .map(
                                                                                (
                                                                                    number
                                                                                ) => {
                                                                                    return (
                                                                                        <li key={number}>
                                                                                            {
                                                                                                number
                                                                                            }
                                                                                        </li>
                                                                                    )
                                                                                }
                                                                            )}
                                                                    </td>
                                                                </tr>
                                                            )
                                                        }
                                                    )}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </>
    )
}

export default WorkoutList
